import { Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const CartComponent = (props) => {
  return (
    <div>
      <nav className='navbar navbar-light bg-body fixed-bottom border'>
        <div className='container-lg' style={{ width: '700px' }}>
          <Navbar.Brand>
            {`Total Belanja : Rp ${parseInt(props.total).toLocaleString()},-`}
          </Navbar.Brand>
          <Link to={props.link} className='btn btn-success'>
            {props.button}
          </Link>
        </div>
      </nav>
    </div>
  );
};

export default CartComponent;
