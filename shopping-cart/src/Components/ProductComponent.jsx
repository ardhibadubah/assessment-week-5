import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Card,
  Button,
  Row,
  Col,
  InputGroup,
  FormControl,
} from 'react-bootstrap';
import { addCart, adjustQty, adjustStok } from '../Redux/action/cartAction';
import CartComponent from './CartComponent';

function ProductComponent(props) {
  const listItem = props.listItem;
  const cart = useSelector((data) => data.cartReducer.cart);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(addCart(listItem));
    // eslint-disable-next-line
  }, [listItem]);

  const handlerQty = (uid, value) => {
    dispatch(adjustQty({ uid, value }));
    dispatch(adjustStok({ uid, value }));
  };

  return (
    <>
      <div className='container my-5 py-4' style={{ maxWidth: '940px' }}>
        <Row
          xs={1}
          sm={2}
          md={3}
          lg={4}
          className='d-flex justify-content-center justify-content-sm-start g-3'>
          {listItem.map((item) => {
            return (
              <Col key={item.uid} xs={12} sm={6} md={4} lg={3}>
                <Card
                  className='shadow-sm p-3 mb-1 bg-body rounded'
                  style={{ width: '100%' }}>
                  <Card.Img
                    variant='top'
                    src={item.image.url}
                    alt={item.image.altText}
                    className='product-img product-grid'
                  />
                  <Card.Body>
                    <Card.Title>{item.productName}</Card.Title>
                    <Card.Text className='mb-2 text-muted'>
                      Stok : {item.stok}
                    </Card.Text>
                    <Card.Subtitle>{`Rp ${parseInt(
                      item.price
                    ).toLocaleString()},-`}</Card.Subtitle>

                    <InputGroup className='mt-4'>
                      <Button
                        style={{ width: '40px' }}
                        variant='success'
                        id='button-addon2'
                        disabled={item.qty <= 0 ? true : false}
                        onClick={() => handlerQty(item.uid, item.qty - 1)}>
                        -
                      </Button>
                      <FormControl
                        min='0'
                        type='number'
                        style={{ textAlign: 'center' }}
                        value={item.qty}
                        onChange={(e) => handlerQty(item.uid, e.target.value)}
                      />

                      <Button
                        style={{ width: '40px' }}
                        variant='success'
                        id='button-addon2'
                        disabled={
                          item.qty >= item.availableQuantity ? true : false
                        }
                        onClick={() => handlerQty(item.uid, item.qty + 1)}>
                        +
                      </Button>
                    </InputGroup>
                  </Card.Body>
                </Card>
              </Col>
            );
          })}
        </Row>
      </div>
      {cart.length && (
        <CartComponent
          button='Checkout'
          link='/checkout'
          total={
            cart.length &&
            cart.map((obj) => obj.qty * obj.price).reduce((a, b) => a + b)
          }
        />
      )}
    </>
  );
}

export default ProductComponent;
