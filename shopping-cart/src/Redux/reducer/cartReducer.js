const initialState = {
  loading: false,
  error: false,
  cart: false,
  listItem: false,
};

const cartReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case 'SET_LIST_ITEM':
      return {
        ...state,
        loading: payload.loading,
        listItem: payload.result,
        error: payload.error,

        // listItem: payload.map((product) => {
        //   return { ...product, qty: 0, stok: product.availableQuantity };
        // }),
      };
    case 'SET_CART':
      return {
        ...state,
        cart: payload.filter((obj) => obj.qty > 0),
      };
    case 'ADJUST_QTY':
      return {
        ...state,
        listItem: state.listItem.map((item) => {
          if (item.uid === payload.uid) {
            return {
              ...item,
              qty: payload.value,
            };
          } else {
            return item;
          }
        }),
      };
    case 'SET_STOK':
      return {
        ...state,
        listItem: state.listItem.map((item) => {
          if (item.uid === payload.uid) {
            return {
              ...item,
              stok: item.availableQuantity - payload.value,
            };
          } else {
            return item;
          }
        }),
      };

    default:
      return {
        ...state,
      };
  }
};

export default cartReducer;
