import axios from 'axios';

export const getListItem = () => {
  return (dispatch) => {
    // loading
    dispatch({
      type: 'SET_LIST_ITEM',
      payload: {
        loading: true,
        result: false,
        error: false,
      },
    });

    // get API
    axios
      .get(
        'https://my-json-server.typicode.com/irhamhqm/placeholder-shops/items'
      )
      .then((response) => {
        // success get API
        dispatch({
          type: 'SET_LIST_ITEM',
          payload: {
            loading: false,
            result: response.data.map((product) => {
              return { ...product, qty: 0, stok: product.availableQuantity };
            }),
            error: false,
          },
        });
      })
      .catch((error) => {
        // failed get API
        dispatch({
          type: 'SET_LIST_ITEM',
          payload: {
            loading: false,
            result: false,
            error: error.message,
          },
        });
      });
  };

  // return async (dispatch) => {
  //   try {
  //     const result = await axios(
  //       'https://my-json-server.typicode.com/irhamhqm/placeholder-shops/items'
  //     );
  //     dispatch({ type: 'SET_LIST_ITEM', payload: result.data });
  //   } catch (err) {
  //     console.error(err);
  //   }
  // };
};

export const addCart = (data) => {
  return {
    type: 'SET_CART',
    payload: data,
  };
};

export const adjustQty = (data) => {
  return {
    type: 'ADJUST_QTY',
    payload: data,
  };
};

export const adjustStok = (data) => {
  return {
    type: 'SET_STOK',
    payload: data,
  };
};
