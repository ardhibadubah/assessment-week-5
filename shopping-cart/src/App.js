import './App.css';
import NavbarComponents from './Components/NavbarComponents';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import CheckoutComponent from './Components/CheckoutComponent';
import HomeComponent from './Components/HomeComponent';

function App() {
  return (
    <>
      <Router className='App'>
        <NavbarComponents />
        <Routes>
          <Route path='/' element={<HomeComponent />} />
          <Route path='/checkout' element={<CheckoutComponent />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
